
const express = require('express')
const app = express()
const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.listen(port, () => console.log(`Server is running at localhost:${port}`))


app.get("/home", (request, response) => {
	response.send('Welcome to the home page')
})


let users = [{
	username: "johndoe",
	password: "johndoe1234"
}]
app.get("/users", (request, response) => {
	response.send(users)
})


app.delete("/delete-user", (request, response) => {
	response.send(`User ${request.body.username} has been deleted`)
})

